package monkeycannon;

import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import engine.SimpleGameObject;

public class Freedom extends SimpleGameObject {

    private Node monkeyHead;
    private Vector3f vel;
    private  float time = 3;

    @Override
    public void simpleInit() {
        monkeyHead = (Node) game.getAssetManager().loadModel("Models/MonkeyHead/MonkeyHead.mesh.j3o");
        attachChild(monkeyHead);

        Vector3f rotation = new Vector3f(FastMath.nextRandomFloat() * FastMath.TWO_PI, FastMath.nextRandomFloat() * FastMath.TWO_PI, 0);
        Matrix3f pitch = new Matrix3f(1, 0, 0, 0, FastMath.cos(rotation.x), -FastMath.sin(rotation.x), 0, FastMath.sin(rotation.x), FastMath.cos(rotation.x));
        Matrix3f yaw = new Matrix3f(FastMath.cos(rotation.y), 0, FastMath.sin(rotation.y), 0, 1, 0, -FastMath.sin(rotation.y), 0, FastMath.cos(rotation.y));
        vel = yaw.mult(pitch).mult(Vector3f.UNIT_Z).mult(52f);
        setRot(rotation);
    }

    @Override
    public void simpleUpdate(float tpf) {
        vel = vel.add(0, 0, 0);
        setPos(pos.add(vel.mult(tpf)));
        time -= tpf;
        if(time < 0) {
            this.game.remove(this);
        }
    }
}
