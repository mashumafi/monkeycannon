package monkeycannon;

import com.jme3.audio.AudioNode;
import com.jme3.font.BitmapText;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.heightmap.HillHeightMap;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;
import com.jme3.util.SkyFactory;
import engine.SimpleGame;

public class MonkeyCannonGame extends SimpleGame {

    public static final float HMAP_X_OFFSET = 256;
    public static final float HMAP_Z_OFFSET = 256;
    protected HillHeightMap heightmap;
    protected Target target;
    private long gameStart;
    private long score;
    private BitmapText kLRangle;
    private BitmapText kUDangle;
    private BitmapText hudText;
    private AudioNode end_level, monster_loop, background;

    public HillHeightMap getHeightmap() {
        return heightmap;
    }

    public static void main(String[] args) {
        SimpleGame app = new MonkeyCannonGame();
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 768);
        settings.setBitsPerPixel(32);
        settings.setFrameRate(60);
        settings.setVSync(true);
        app.setSettings(settings);
        app.start();
    }

    @Override
    public void simpleInitApp() {
        super.simpleInitApp();
        initLight();
        initTerrain();
        initModels();
        initCoordCross();
        initAudio();
        this.getRenderer().setBackgroundColor(ColorRGBA.Blue);
        gameStart = System.currentTimeMillis();
        score = -1;
        //some text displays for seeing what the kinect is doing
        kLRangle = new BitmapText(guiFont, false);
        kLRangle.setSize(guiFont.getCharSet().getRenderedSize() * 2);      // font size
        kLRangle.setColor(ColorRGBA.Blue);                             // font color
        kLRangle.setLocalTranslation(30, kLRangle.getLineHeight() + 700, 0); // position
        guiNode.attachChild(kLRangle);
        kUDangle = new BitmapText(guiFont, false);
        kUDangle.setSize(guiFont.getCharSet().getRenderedSize() * 2);      // font size
        kUDangle.setColor(ColorRGBA.Blue);                             // font color
        kUDangle.setLocalTranslation(30, kUDangle.getLineHeight() + 670, 0); // position
        guiNode.attachChild(kUDangle);
        //generic message area(mainly the gameover message)
        hudText = new BitmapText(guiFont, false);
        hudText.setSize(guiFont.getCharSet().getRenderedSize() * 2);      // font size
        hudText.setColor(ColorRGBA.Blue);                             // font color
        hudText.setLocalTranslation(250, hudText.getLineHeight() + 300, 0); // position
        guiNode.attachChild(hudText);
    }
    float monkey_gen = 0f;

    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);

        if (target.isDead()) {
            monkey_gen += tpf;
            while (monkey_gen > 0.03125f / 2f) {
                Freedom freedom = new Freedom();
                freedom.setPos(target.getPos());
                add(freedom);
                monkey_gen -= 0.03125f / 2f;
            }
            if (score < 0) {
                monster_loop.stop();
                background.stop();
                end_level.play();

                score = System.currentTimeMillis() - gameStart;
                hudText.setText("You killed Oto in " + score / 1000.0 + " seconds!");
            }
        }
    }

    public void initModels() {
        float randX;
        float randZ;

        /**
         * Setup the player's cannon
         */
        Cannon player = new Cannon();
        randX = FastMath.nextRandomFloat() * heightmap.getSize() - HMAP_X_OFFSET;
        randZ = FastMath.nextRandomFloat() * heightmap.getSize() - HMAP_Z_OFFSET;
        player.setPos(new Vector3f(randX, 0, randZ).setY(heightmap.getInterpolatedHeight(randX + HMAP_X_OFFSET, randZ + HMAP_Z_OFFSET) + 8f));
        add(player);

        /**
         * Set up the camera
         */
        Vector3f position = Vector3f.UNIT_Z.mult(-30);
        Vector3f rotation = player.getRot();
        position = position.add(Vector3f.UNIT_Y.mult(2));
        Matrix3f yaw = new Matrix3f(FastMath.cos(rotation.y), 0, FastMath.sin(rotation.y), 0, 1, 0, -FastMath.sin(rotation.y), 0, FastMath.cos(rotation.y));
        position = yaw.mult(position);
        position = position.add(player.getPos());
        position = position.add(0, 8, 0);
        Camera camera = getCamera();
        camera.setLocation(position);
        Vector3f newPos = new Vector3f();
        newPos = newPos.add(0, 0, 30);
        newPos = yaw.mult(newPos);
        newPos = position.add(newPos);
        camera.lookAt(newPos, Vector3f.UNIT_Y);

        /**
         * Setup Oto the doomed robot
         */
        target = new Target();
        randX = FastMath.nextRandomFloat() * heightmap.getSize() - HMAP_X_OFFSET;
        randZ = FastMath.nextRandomFloat() * heightmap.getSize() - HMAP_Z_OFFSET;
        target.setPos(new Vector3f(randX, 0, randZ).setY(heightmap.getInterpolatedHeight(randX + HMAP_X_OFFSET, randZ + HMAP_Z_OFFSET) + 8.4f));
        add(target);
    }

    public void initLight() {
        /**
         * A white ambient light source.
         */
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.White.mult(1.5f));
        rootNode.addLight(ambient);
        /**
         * A white, directional light source
         */
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(-0.5f, -0.5f, -0.5f)).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);
    }

    public void initTerrain() {
        /**
         * Some randomized terrain
         */
        /**
         * 1. Create terrain material and load four textures into it.
         */
        Material mat_terrain = new Material(assetManager, "Common/MatDefs/Terrain/Terrain.j3md");

        /**
         * 1.1) Add ALPHA map (for red-blue-green coded splat textures)
         */
        mat_terrain.setTexture("Alpha", assetManager.loadTexture("Textures/Terrain/splat/alphamap.png"));

        /**
         * 1.2) Add GRASS texture into the red layer (Tex1).
         */
        Texture grass = assetManager.loadTexture("Textures/Terrain/splat/grass.jpg");
        grass.setWrap(WrapMode.Repeat);
        mat_terrain.setTexture("Tex1", grass);
        mat_terrain.setFloat("Tex1Scale", 64f);

        /**
         * 1.3) Add DIRT texture into the green layer (Tex2)
         */
        Texture dirt = assetManager.loadTexture("Textures/Terrain/splat/dirt.jpg");
        dirt.setWrap(WrapMode.Repeat);
        mat_terrain.setTexture("Tex2", dirt);
        mat_terrain.setFloat("Tex2Scale", 32f);

        /**
         * 1.4) Add ROAD texture into the blue layer (Tex3)
         */
        Texture rock = assetManager.loadTexture("Textures/Terrain/splat/road.jpg");
        rock.setWrap(WrapMode.Repeat);
        mat_terrain.setTexture("Tex3", rock);
        mat_terrain.setFloat("Tex3Scale", 128f);
        /**
         * 2. Create the height map
         */
        HillHeightMap.NORMALIZE_RANGE = 100; // optional
        try {
            heightmap = new HillHeightMap(513, 1000, 600, 800, System.currentTimeMillis());
            heightmap.load();
            int patchSize = 65;
            TerrainQuad terrain = new TerrainQuad("my terrain", patchSize, 513, heightmap.getHeightMap());
            terrain.setMaterial(mat_terrain);
            terrain.setLocalTranslation(0, 0, 0);
            rootNode.attachChild(terrain);

            TerrainLodControl control = new TerrainLodControl(terrain, getCamera());
            terrain.addControl(control);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        rootNode.attachChild(SkyFactory.createSky(assetManager, "Textures/Sky/Bright/BrightSky.dds", false));
    }

    private void initCoordCross() {
        Material matRed = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matRed.setColor("Color", ColorRGBA.Red);
        Material matGreen = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matGreen.setColor("Color", ColorRGBA.Green);
        Material matBlue = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matBlue.setColor("Color", ColorRGBA.Blue);

        float origin_height = heightmap.getInterpolatedHeight(0 + HMAP_X_OFFSET, 0 + HMAP_Z_OFFSET) + 1f;
        Node eOrigin = new Node();
        eOrigin.setLocalTranslation(0, origin_height + 8, 0);
        rootNode.attachChild(eOrigin);
    }

    public void initAudio() {
        AudioNode explosion = new AudioNode(assetManager, "Sounds/explosion.ogg", false);
        explosion.setName("explosion");
        explosion.setVolume(.3f);
        rootNode.attachChild(explosion);

        AudioNode shoot = new AudioNode(assetManager, "Sounds/shoot.wav", false);
        shoot.setName("shoot");
        rootNode.attachChild(shoot);

        AudioNode monster1 = new AudioNode(assetManager, "Sounds/monster1.ogg", false);
        monster1.setName("monster1");
        rootNode.attachChild(monster1);

        AudioNode monster2 = new AudioNode(assetManager, "Sounds/monster2.ogg", false);
        monster2.setName("monster2");
        rootNode.attachChild(monster2);

        AudioNode scream = new AudioNode(assetManager, "Sounds/scream.wav", false);
        scream.setName("scream");
        scream.setVolume(.5f);
        rootNode.attachChild(scream);

        monster_loop = new AudioNode(assetManager, "Sounds/monster_loop.wav", false);
        monster_loop.setName("monster_loop");
        monster_loop.setLooping(true);
        monster_loop.setVolume(.1f);
        monster_loop.setPositional(true);
        monster_loop.play();
        rootNode.attachChild(monster_loop);

        background = new AudioNode(assetManager, "Sounds/bluebeat.ogg", false);
        background.setLooping(true);
        background.setVolume(.25f);
        background.play();
        rootNode.attachChild(background);

        end_level = new AudioNode(assetManager, "Sounds/end_level.ogg", false);
        rootNode.attachChild(end_level);
    }
}
