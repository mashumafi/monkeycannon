package monkeycannon;

import com.jme3.audio.AudioNode;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import engine.SimpleGameObject;

public class Cannon extends SimpleGameObject {

    public static final float DEADZONE_WIDTH = 1.0f;
    public static final float DEADZONE_HEIGHT = FastMath.QUARTER_PI;
    public static final float CHARGE_THRESHOLD = FastMath.PI;
    public static final float MIN_PITCH = -FastMath.HALF_PI;
    public static final float MAX_PITCH = FastMath.HALF_PI;
    public static final float KB_TURN_REDUCTION = 5.5f;
    public static final float KINECT_TURN_REDUCTION = 7f;
    public static final long REFIRE_DELAY = 500;
    Node hoverTank;
    long lastFire;
    private boolean readyToFire = false;
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean pressed, float tpf) {
        }
    };
    private AnalogListener analogListener = new AnalogListener() {
        public void onAnalog(String name, float value, float tpf) {
            float speed = FastMath.PI / KB_TURN_REDUCTION * tpf;
            if (name.equals("Left")) {
                Cannon.this.yawLeft(speed);
            }
            if (name.equals("Right")) {
                Cannon.this.yawRight(speed);
            }
            if (name.equals("Up")) {
                Cannon.this.pitchUp(speed);
            }
            if (name.equals("Down")) {
                Cannon.this.pitchDown(speed);
            }
            if (name.equals("Fire")) {
                Cannon.this.fireCannon();
            }
            Cannon.this.adjustCamera();
        }
    };

    public Cannon() {
        lastFire = System.currentTimeMillis();
    }

    @Override
    public void simpleInit() {
        hoverTank = (Node) game.getAssetManager().loadModel("Models/HoverTank/tankFinalExport.j3o");

        attachChild(hoverTank);

        game.getInputManager().addMapping("Left", new KeyTrigger(KeyInput.KEY_J));
        game.getInputManager().addMapping("Right", new KeyTrigger(KeyInput.KEY_L));
        game.getInputManager().addMapping("Up", new KeyTrigger(KeyInput.KEY_I));
        game.getInputManager().addMapping("Down", new KeyTrigger(KeyInput.KEY_K));
        game.getInputManager().addMapping("Fire", new KeyTrigger(KeyInput.KEY_SPACE));
        game.getInputManager().addListener(actionListener, new String[]{"Left", "Right", "Up", "Down", "Fire"});
        game.getInputManager().addListener(analogListener, new String[]{"Left", "Right", "Up", "Down", "Fire"});
    }

    @Override
    public void simpleUpdate(float tpf) {
        if (game.getKinect().getSkeletonCount() >= 1) {
            Vector3f rs = game.getKinect().getRightShoulder(0);
            Vector3f rw = game.getKinect().getRightWrist(0);
            Vector3f lw = game.getKinect().getLeftWrist(0);
            Vector3f ls = game.getKinect().getLeftShoulder(0);
            float speed = FastMath.PI / KINECT_TURN_REDUCTION * tpf;
            if (rs != null && rw != null) {
                Vector3f v = rw.subtract(rs);
                float left_right = atan2(v.x, v.z);
                float up_down = atan2(v.y, v.z);
                //((MonkeyCannonGame) game).kLRangle.setText("LR angle: " + left_right);
                //((MonkeyCannonGame) game).kUDangle.setText("UD angle: " + up_down);
                if (up_down < FastMath.PI - DEADZONE_HEIGHT / 2.0f) {
                    pitchUp(speed);
                }
                if (up_down > FastMath.PI + DEADZONE_HEIGHT / 2.0f) {
                    pitchDown(speed);
                }
                if (left_right < FastMath.PI - DEADZONE_WIDTH / 2.0f) {
                    float turn_mod = FastMath.PI - left_right - DEADZONE_WIDTH / 2.0f;
                    turn_mod = FastMath.pow(turn_mod, 5.0f);
                    turn_mod /= 75;
                    //((MonkeyCannonGame) game).hudText.setText("turn/speed: " + turn_mod + "/" + speed);
                    yawRight(turn_mod + speed);
//                    if (left_right < FastMath.PI - DEADZONE_WIDTH) {
//                        yawRight(speed * 3f);
//                    } else {
//                        yawRight(speed);
//                    }
                }
                if (left_right > FastMath.PI + DEADZONE_WIDTH / 2.0f) {
                    float turn_mod = left_right - (FastMath.PI + DEADZONE_WIDTH / 2.0f);
                    turn_mod = FastMath.pow(turn_mod, 5.0f);
                    turn_mod /= 75;
                    //((MonkeyCannonGame) game).hudText.setText("turn/speed: " + turn_mod + "/" + speed);
                    yawLeft(turn_mod + speed);
//                    if (left_right > FastMath.PI + DEADZONE_WIDTH) {
//                        yawLeft(speed * 3f);
//                    } else {
//                        yawLeft(speed);
//                    }
                }
                adjustCamera();
            }
            if (ls != null && lw != null) {
                Vector3f v = lw.subtract(ls);
                float charge_arm = atan2(v.y, v.z);
                //((MonkeyCannonGame) game).hudText.setText("Charge arm: " + charge_arm);
                if (charge_arm < CHARGE_THRESHOLD) {
                    readyToFire = true;
                } else {
                    if (readyToFire) {
                        readyToFire = false;
                        fireCannon();
                    }
                }
            }
        }
    }

    @Override
    public void setRot(Vector3f rot) {
        this.rot = rot;
        setLocalRotation(Matrix3f.IDENTITY);
        rotate(0, rot.y, 0);
        hoverTank.setLocalRotation(Matrix3f.IDENTITY);
        hoverTank.rotate(rot.x, 0, 0);
    }

    public static float atan2(float y, float x) {
        double theta = Math.atan(y / x);
        if (x < 0) {
            theta += Math.PI;
        }
        if (theta < 0) {
            theta += 2 * Math.PI;
        }
        return (float) theta;
    }

    private void yawRight(float speed) {
        setRot(new Vector3f(rot.x, rot.y - speed, 0));
    }

    private void yawLeft(float speed) {
        setRot(new Vector3f(rot.x, rot.y + speed, 0));
    }

    private void pitchUp(float speed) {
        if (rot.x - speed > MIN_PITCH) {
            setRot(new Vector3f(rot.x - speed, rot.y, 0));
        }
    }

    private void pitchDown(float speed) {
        if (rot.x + speed < MAX_PITCH) {
            setRot(new Vector3f(rot.x + speed, rot.y, 0));
        }
    }

    private void fireCannon() {
        long time = System.currentTimeMillis();
        if (time - lastFire > REFIRE_DELAY) {
            game.add(new CannonBall(this));
            lastFire = time;

            AudioNode audio = (AudioNode) game.getRootNode().getChild("shoot");
            AudioNode shoot = audio.clone();
            shoot.setPositional(true);
            shoot.setLocalTranslation(this.pos);
            shoot.setVolume(0.25f);
            game.getRootNode().attachChild(shoot);
            shoot.play();
        }
    }

    private void adjustCamera() {
        Vector3f position = Vector3f.UNIT_Z.mult(-60);
        Vector3f rotation = getRot();
        position = position.add(Vector3f.UNIT_Y.mult(10));
        Matrix3f yaw = new Matrix3f(FastMath.cos(rotation.y), 0, FastMath.sin(rotation.y), 0, 1, 0, -FastMath.sin(rotation.y), 0, FastMath.cos(rotation.y));
        position = yaw.mult(position);
        position = position.add(getPos());
        position = position.add(0, 8, 0);
        Camera cam = game.getCamera();
        cam.setLocation(position);
        Vector3f newPos = new Vector3f();
        newPos = newPos.add(0, 0, 30);
        newPos = yaw.mult(newPos);
        newPos = position.add(newPos);
        cam.lookAt(newPos, Vector3f.UNIT_Y.clone());
    }
}
