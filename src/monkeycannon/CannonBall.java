package monkeycannon;

import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingVolume;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.control.LightControl;
import com.jme3.scene.shape.Sphere;
import com.jme3.terrain.heightmap.HillHeightMap;
import engine.SimpleGameObject;

public class CannonBall extends SimpleGameObject {

    public static final float PROJECTILE_SPEED = 13f;
    public static final float GRAVITY = -9.8f;
    private Node monkeyHead;
    private Geometry target;
    private Vector3f velocity;
    private Cannon cannon;
    private PointLight glow;

    public CannonBall(Cannon cannon) {
        this.cannon = cannon;
    }

    @Override
    public void simpleInit() {
        monkeyHead = (Node) game.getAssetManager().loadModel("Models/MonkeyHead/MonkeyHead.mesh.j3o");
        attachChild(monkeyHead);

        glow = new PointLight();
        glow.setColor(ColorRGBA.Orange.mult(8f));
        glow.setRadius(1000f);
        LightControl lc = new LightControl(glow);
        monkeyHead.addControl(lc);

        target = new Geometry("target", new Sphere(5, 5, 1.0f));
        Material mat = new Material(game.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Green);
        target.setMaterial(mat);
        attachChild(target);

        Vector3f position = Vector3f.UNIT_Z.mult(6);
        Vector3f rotation = cannon.getRot();
        position = position.add(Vector3f.UNIT_Y.mult(2));
        Matrix3f pitch = new Matrix3f(1, 0, 0, 0, FastMath.cos(rotation.x), -FastMath.sin(rotation.x), 0, FastMath.sin(rotation.x), FastMath.cos(rotation.x));
        Matrix3f yaw = new Matrix3f(FastMath.cos(rotation.y), 0, FastMath.sin(rotation.y), 0, 1, 0, -FastMath.sin(rotation.y), 0, FastMath.cos(rotation.y));
        position = yaw.mult(pitch).mult(position);
        velocity = position.mult(PROJECTILE_SPEED);
        position = position.add(cannon.getPos());
        setPos(position);
        setRot(rotation);
        monkeyHead.scale(1.0f);
    }

    @Override
    public void setPos(Vector3f pos) {
        this.pos = pos;
        monkeyHead.setLocalTranslation(pos);
    }

    @Override
    public void setRot(Vector3f rot) {
        this.rot = rot;
        monkeyHead.setLocalRotation(Matrix3f.IDENTITY);
        monkeyHead.rotate(rot.x, rot.y, rot.z);
    }

    @Override
    public void simpleUpdate(float tpf) {
        this.pos.x += this.velocity.x * tpf;
        this.pos.y += this.velocity.y * tpf;
        this.pos.z += this.velocity.z * tpf;
        this.setPos(pos);
        this.velocity.y = this.velocity.y + GRAVITY * tpf;
        HillHeightMap hhm = ((MonkeyCannonGame) game).getHeightmap();
        float height = 0;
        if (pos.x <= -hhm.getSize() / 2
                || pos.x >= hhm.getSize() / 2
                || pos.z <= -hhm.getSize() / 2
                || pos.z >= hhm.getSize() / 2
                || (height = hhm.getInterpolatedHeight(pos.x + MonkeyCannonGame.HMAP_X_OFFSET, pos.z + MonkeyCannonGame.HMAP_Z_OFFSET)) >= pos.y) {
            explode("scream");
            game.remove(this);
        }
        target.setLocalTranslation(pos.x, height, pos.z);
        checkCollision();
    }

    private void explode(String name) {
        AudioNode audio = (AudioNode) game.getRootNode().getChild(name);
        AudioNode explosion = audio.clone();
        explosion.setPositional(true);
        explosion.setLocalTranslation(this.pos);
        game.getRootNode().attachChild(explosion);
        explosion.play();
        
        // add model of explosion
    }

    public void checkCollision() {
        Target oto = ((MonkeyCannonGame) game).target;
        BoundingVolume oto_bv = oto.getWorldBound();
        BoundingVolume monkey_bv = monkeyHead.getWorldBound();
        if (oto_bv.intersects(monkey_bv)) {
            game.remove(this);
            explode("explosion");
            oto.explode();
        }
    }
}
