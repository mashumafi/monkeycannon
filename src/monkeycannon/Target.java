package monkeycannon;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import engine.SimpleGameObject;

public class Target extends SimpleGameObject implements AnimEventListener {

    public static final float MOVE_SPEED = 5f;
    public static final float BUFFER = 10;
    Node oto;
    private int hits;
    private boolean burning = false;
    private boolean alive = true;
    private AnimChannel otoAnim;
    float facing;

    public boolean isDead() {
        return !alive;
    }

    @Override
    public void simpleInit() {
        oto = (Node) game.getAssetManager().loadModel("Models/Oto/Oto.mesh.j3o");
        attachChild(oto);
        AnimControl control = oto.getControl(AnimControl.class);
        control.addListener(this);
        otoAnim = control.createChannel();
        otoAnim.setAnim("Walk");
        facing = FastMath.nextRandomFloat() * FastMath.TWO_PI;
        oto.scale(2.0f);
    }

    @Override
    public void simpleUpdate(float tpf) {
        if (alive) {
            Vector3f position = getLocalTranslation();
            if (position.x < -MonkeyCannonGame.HMAP_X_OFFSET + BUFFER) {
                facing = FastMath.nextRandomFloat() * FastMath.PI;
                facing = FastMath.clamp(facing, FastMath.HALF_PI * 1f / 5f, FastMath.HALF_PI * 9f / 5f);
            } else if (position.x > MonkeyCannonGame.HMAP_X_OFFSET - BUFFER) {
                facing = FastMath.nextRandomFloat() * FastMath.PI + FastMath.PI;
                facing = FastMath.clamp(facing, FastMath.HALF_PI * 11f / 5f, FastMath.HALF_PI * 19f / 5f);
            }
            if (position.z < -MonkeyCannonGame.HMAP_Z_OFFSET + BUFFER) {
                facing = FastMath.nextRandomFloat() * FastMath.PI - FastMath.HALF_PI;
                facing = FastMath.clamp(facing, -FastMath.HALF_PI * 4f / 5f, FastMath.HALF_PI * 4f / 5f);
            } else if (position.z > MonkeyCannonGame.HMAP_Z_OFFSET - BUFFER) {
                facing = FastMath.nextRandomFloat() * FastMath.PI + FastMath.HALF_PI;
                facing = FastMath.clamp(facing, FastMath.HALF_PI * 6f / 5f, FastMath.HALF_PI * 9f / 5f);
            }
            setRot(new Vector3f(0, facing, 0));
            float xspeed = FastMath.sin(getRot().y) * MOVE_SPEED * tpf;
            float zspeed = FastMath.cos(getRot().y) * MOVE_SPEED * tpf;
            position = position.add(xspeed, 0, zspeed);
            position.setY(((MonkeyCannonGame) game).getHeightmap().getInterpolatedHeight(position.x + MonkeyCannonGame.HMAP_X_OFFSET, position.z + MonkeyCannonGame.HMAP_Z_OFFSET) + 8.4f);
            setPos(position);


            AudioNode monster_loop = (AudioNode) game.getRootNode().getChild("monster_loop");
            monster_loop.setLocalTranslation(this.pos);
        }
    }

    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
        if (alive) {
            channel.setAnim("Walk");
        } else {
            channel.setAnim("stand");
        }
    }

    public void explode() {
        if (!burning) {
            ParticleEmitter fire = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 60);
            Material mat_red = new Material(game.getAssetManager(), "Common/MatDefs/Misc/Particle.j3md");
            mat_red.setTexture("Texture", game.getAssetManager().loadTexture("Effects/Explosion/flame.png"));
            fire.setMaterial(mat_red);
            fire.setImagesX(2);
            fire.setImagesY(2); // 2x2 texture animation
            fire.setEndColor(new ColorRGBA(1f, 0f, 0f, 1f));   // red
            fire.setStartColor(new ColorRGBA(1f, 1f, 0f, 0.5f)); // yellow
            fire.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 2, 0));
            fire.setStartSize(2.0f);
            fire.setEndSize(0.2f);
            fire.setGravity(0f, -0.1f, 0f);
            fire.setLowLife(1.5f);
            fire.setHighLife(10f);
            fire.getParticleInfluencer().setVelocityVariation(0.3f);
            fire.setLocalTranslation(0, 9.0f, 0.7f);
            attachChild(fire);

            burning = true;
        }
        hits++;
        if (hits >= 5) {
            alive = false;
        }
        otoAnim.setAnim("Dodge");
        otoAnim.setSpeed(0.5f);
    }

    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
    }
}
