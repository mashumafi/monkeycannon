package engine;

import com.jme3.math.Vector3f;
import kinecttcpclient.KinectTCPClient;

public class Kinect {

    private KinectTCPClient client;
    private int[][][] joints = new int[6][][];
    public static final int HipCenter = 0,
            Spine = 1,
            ShoulderCenter = 2,
            Head = 3,
            ShoulderLeft = 4,
            ElbowLeft = 5,
            WristLeft = 6,
            HandLeft = 7,
            ShoulderRight = 8,
            ElbowRight = 9,
            WristRight = 10,
            HandRight = 11,
            HipLeft = 12,
            KneeLeft = 13,
            AnkleLeft = 14,
            FootLeft = 15,
            HipRight = 16,
            KneeRight = 17,
            AnkleRight = 18,
            FootRight = 19;
    private int skelecount = 0;

    public Kinect() {
        this("127.0.0.1", 8001);
    }

    public Kinect(String ipaddress, int port) {
        try {
            client = new KinectTCPClient(ipaddress, port);
        } catch (Throwable e) {
        }
    }

    private boolean isValid(int skeletonIndex, int jointIndex) {
        return joints != null
                && skeletonIndex < joints.length
                && joints[skeletonIndex] != null
                && jointIndex < joints[skeletonIndex].length
                && joints[skeletonIndex][jointIndex] != null
                && joints[skeletonIndex][jointIndex].length >= 3;
    }

    private Vector3f getPosition(int skeletonIndex, int jointIndex) {
        if (isValid(skeletonIndex, jointIndex)) {
            return new Vector3f(joints[skeletonIndex][jointIndex][1], joints[skeletonIndex][jointIndex][2], joints[skeletonIndex][jointIndex][3]);
        }
        return null;
    }

    public void update() {
        skelecount = 0;
        if (client != null) {
            int[] skeleton = client.readSkeleton();
            if (skeleton != null) {
                for (int i = 1; i <= skeleton[0]; i++) { // (skeleton.length - 17) / 149
                    joints[i - 1] = KinectTCPClient.getJointPositions(skeleton, i);
                    skelecount = i;
                }
            }
        }
    }

    public int getSkeletonCount() {
        return skelecount;
    }

    public Vector3f getRightShoulder(int skeletonIndex) {
        return getPosition(skeletonIndex, ShoulderRight);
    }

    public Vector3f getRightWrist(int skeletonIndex) {
        return getPosition(skeletonIndex, WristRight);
    }

    public Vector3f getRightHand(int skeletonIndex) {
        return getPosition(skeletonIndex, HandRight);
    }

    public Vector3f getLeftShoulder(int skeletonIndex) {
        return getPosition(skeletonIndex, ShoulderLeft);
    }

    public Vector3f getLeftWrist(int skeletonIndex) {
        return getPosition(skeletonIndex, WristLeft);
    }

    public Vector3f getLeftHand(int skeletonIndex) {
        return getPosition(skeletonIndex, HandLeft);
    }
}
