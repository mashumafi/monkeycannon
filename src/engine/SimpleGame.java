package engine;

import com.jme3.app.SimpleApplication;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;

public abstract class SimpleGame extends SimpleApplication {

    protected Kinect kinect;

    public SimpleGame() {
        this.setShowSettings(true);
        this.setDisplayStatView(false);
        this.setDisplayFps(true);
        this.setPauseOnLostFocus(false);
    }

    @Override
    public void simpleInitApp() {
        flyCam.setEnabled(true);
        flyCam.setMoveSpeed(150);
        kinect = new Kinect();
    }

    public void add(SimpleGameObject sgo) {
        if (!sgo.isInitialized()) {
            sgo.setGame(this);
            sgo.simpleInit();
        }
        rootNode.attachChild(sgo);
    }

    public void remove(SimpleGameObject sgo) {
        rootNode.detachChild(sgo);
    }

    @Override
    public void simpleUpdate(final float tpf) {
        kinect.update();
        SceneGraphVisitor visitor = new SceneGraphVisitor() {
            @Override
            public void visit(Spatial spatial) {
                if (spatial instanceof SimpleGameObject) {
                    SimpleGameObject sge = (SimpleGameObject) spatial;
                    sge.simpleUpdate(tpf);
                }
            }
        };
        rootNode.depthFirstTraversal(visitor);
    }

    public Kinect getKinect() {
        return kinect;
    }
}
