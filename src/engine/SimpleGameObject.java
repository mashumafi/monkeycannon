package engine;

import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public abstract class SimpleGameObject extends Node {

    protected SimpleGame game;
    protected Vector3f pos, rot;

    public SimpleGameObject() {
        pos = Vector3f.ZERO.clone();
        rot = Vector3f.ZERO.clone();
    }

    public Vector3f getRot() {
        return rot;
    }

    public void setRot(Vector3f rot) {
        this.rot = rot;
        this.setLocalRotation(Matrix3f.IDENTITY);
        this.rotate(rot.x, rot.y, rot.z);
    }

    public SimpleGame getGame() {
        return game;
    }

    public void setGame(SimpleGame game) {
        this.game = game;
    }

    public Vector3f getPos() {
        return pos;
    }

    public void setPos(Vector3f pos) {
        this.pos = pos;
        this.setLocalTranslation(pos);
    }

    public abstract void simpleInit();

    public abstract void simpleUpdate(float tpf);

    public final boolean isInitialized() {
        return game != null;
    }
}
